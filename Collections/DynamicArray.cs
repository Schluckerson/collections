﻿using System;
using System.Collections;
using System.Collections.Generic;
using Collections.Interfaces;

namespace Collections
{
	public class DynamicArray<T> : IDynamicArray<T>
	{
		private int _length = 0;
		private int _capacity;
		private T[] array;
		public DynamicArray()
		{
			_capacity = 8;
		}

		public DynamicArray(int capacity)
		{
			_capacity = capacity;
			array = new T[Capacity];
		}

		public DynamicArray(IEnumerable<T> items)
		{
			_capacity = CountArr(items);
			array = new T[_capacity];

			foreach (var item in items)
            {
				Add(item);
            }
		}

		public IEnumerator<T> GetEnumerator()
		{
			return ((IEnumerable<T>)array).GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public T this[int index]
        {
			get
			{
				if (index >= Length || index < 0) throw new IndexOutOfRangeException("index");
				return array[index];
			}
			set => array[index] = value;
        }

		public int Length
        {
			get => _length;
        }
		public int Capacity
		{
			get => _capacity;
			set => _capacity = value;
		}
		public void Add(T item)
		{
			CapacityIncrease(1);			
			array[Length] = item;
			_length++;
		}

		

		public void AddRange(IEnumerable<T> items)
		{
			int len = CountArr(items);
			CapacityIncrease(len);
			foreach (var item in items)
            {
				Add(item);
            }
		}

		public void Insert(T item, int index)
		{
			CapacityIncrease(1);
			for (int i = _length; i > index ; i--)
            {
				array[i] = array[i - 1];
            }
			array[index] = item;
		}

		public bool Remove(T item)
		{
			bool changed = false;
			for (int i = 0; i < _length; i++)
            {
				if (array[i] == null) continue;
				if (array[i].GetHashCode() == item.GetHashCode())
				{
					array[i] = default(T);					
					for (int j = i + 1; j < _length - 1; j++)
                    {
						array[j] = array[j + 1];
                    }					
					changed = true;
				}
			}
			_length = Length - 1;
			return changed;
		}

		private void CapacityIncrease(int len)
		{
			bool enough = false;
			bool changed = false;
			while (!enough)
			{
				if (Length + len > Capacity)
				{
					if (_capacity == 0) _capacity++;
					else _capacity *= 2;
					changed = true;
				}
				else enough = true;
			}
			if (changed)
			{
				T[] temp = array;
				array = new T[Capacity];
				int index = 0;
				for (int i = 0; i < _length; i++)
				{
					array[index] = temp[i];
				}
			}
		}

		private int CountArr(IEnumerable<T> arr)
        {
			int count = 0;
			foreach (T item in arr)
            {
				count++;
            }
			return count;
        }
	}
}